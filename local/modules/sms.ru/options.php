<?php

/** @global CMain $APPLICATION */
use Bitrix\Main\Localization\Loc;

CModule::IncludeModule($mid);
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/options.php');
$module_id = $mid;
$bReadOnly = !$USER->CanDoOperation('catalog_settings');

if (!$USER->CanDoOperation('catalog_read') && $bReadOnly) {
    return;
}

$params = [
    [
        'tab' => [
            "DIV"   => "tab_base_settings",
            "TAB"   => Loc::getMessage('SMSRU_OPTIONS_TAB_TITLE'),
            "TITLE" => Loc::getMessage("SMSRU_OPTIONS_TAB_TITLE"),
        ],
        'options' => [
            'api_id' => [
                'title' => Loc::getMessage('SMSRU_OPTIONS_API_LABEL'),
                'field' => ['type' => "text", 'params' => ['size' => 100]],
            ],
            'phone' => [
                'title' => Loc::getMessage('SMSRU_OPTIONS_PHONE_LABEL'),
                'field' => ['type' => "text", 'params' => ['size' => 100]],
            ],
            'tg_token' => [
                'title' => Loc::getMessage('SMSRU_OPTIONS_TG_TOKEN_LABLE'),
                'field' => ['type' => "text", 'params' => ['size' => 100]],
            ],
            'tg_chat' => [
                'title' => Loc::getMessage('SMSRU_OPTIONS_TG_CHAT_ID_LABEL'),
                'field' => ['type' => "text", 'params' => ['size' => 100]],
            ],
        ],
    ],
];

$aTabs = array_column($params, 'tab');
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();

if ($context->getServer()->getRequestMethod() == 'POST' && check_bitrix_sessid()) {
    foreach ($params as $tabData)
    {
        foreach ($tabData['options'] as $optionName => $optionValue)
        {
            if (array_key_exists('field', $optionValue)) {
                COption::SetOptionString(
                    $mid, 
                    $optionName, 
                    htmlspecialcharsbx($request->get($optionName))
                );
            } else {
                foreach ($optionValue['fields'] as $field)
                {
                    COption::SetOptionString(
                        $mid, 
                        $field['name'], 
                        htmlspecialcharsbx($request->get($field['name']))
                    );
                }
            }
        }
    }
}

$urlParams = [
    'mid'   => $mid,
    'lang'  => LANGUAGE_ID,
];
$formActionUrl = $APPLICATION->GetCurPage() . '?' . http_build_query($urlParams);
$MOD_RIGHT = $APPLICATION->GetGroupRight($mid);
?>

<form method="post" action="<?= $formActionUrl ?>">
    <?= bitrix_sessid_post(); ?>
    <?php $tabControl->Begin(); ?>

    <?php foreach ($params as $tabData): ?>
        <?php
            $tabControl->BeginNextTab();
            $groups = $tabData['groups'] ?? ['' => array_keys($tabData['options'])];
        ?>

        <?php foreach ($groups as $title => $options): ?>
            <?php if ($title): ?>
                <tr class="heading">
                    <td colspan="3"><b><?= $title ?></b></td>
                </tr>
        <?php endif; ?>
            <?
            $api_id = COption::GetOptionString($mid, 'api_id');
            if (!empty($api_id)){
                echo '<pre>';
                $smsru = new SMSRU($api_id);
                $request = $smsru->getBalance();
                if ($request->status == "OK") { // Запрос выполнен успешно
                    echo "<span style='color: green'>Ваш баланс: $request->balance </span>";
                } else {
                    echo "<span style='color: red;'>Ошибка при выполнении запроса. </span>";
                    echo "Код ошибки: $request->status_code. ";
                    echo "Текст ошибки: $request->status_text. ";
                }
                echo '</pre>';
            }
            ?>
            <?php foreach ($options as $optionKey => $optionName): ?>
                <?php if ($optionKey === 'custom_header'): ?>
                    <tr>
                        <?php foreach ($optionName as $name): ?>
                            <td >
                                <label >
                                    <?= $name ?>
                                </label>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endif; ?>
                    
                <?php $option = $tabData['options'][$optionName]; ?>
                    
                <?php if (array_key_exists('field', $option)): ?>
                    <?php
                        $field = $option['field'];
                        $value = COption::GetOptionString($mid, $optionName);
                        $params = [
                            'type="' . $field['type'] . '"',
                            'id="' . $optionName . '"',
                            'name="' . $optionName . '"',
                        ];
                        
                        foreach ($field['params'] as $paramName => $paramValue) {
                            $params[] = is_int($paramName) ? $paramValue : "{$paramName}=\"{$paramValue}\"";
                        }
                        
                        $hintTagAttr = null;
                        if (isset($option['hint'])) {
                            $hintTagAttr = 'title="' . $option['hint'] . '"';
                            $params[] = $hintTagAttr;
                            $hintTagAttr .= ' ' . $hintTagAttr;
                        }

                        if (in_array($field['type'], ['text', 'number', 'checkbox'])) {
                            if ($field['type'] === 'checkbox' && $value) {
                                $params[] = 'checked';
                            }
                            if (empty($value) && $field['type'] === 'checkbox') {
                                $value = 1;
                            }
                            $params[] = 'value="' . $value . '"';
                        }
                    ?>

                    <tr>
                        <td width="40%">
                            <label for="<?= $optionName ?>"<?= $hintTagAttr ?>>
                                <?= $option['title'] ?>
                            </label>:
                        </td>
                        <td colspan="2" width="60%">
                            <?php if (in_array($field['type'], ['text', 'number', 'checkbox'])): ?>
                                <input <?= implode(' ', $params) ?>>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php elseif (array_key_exists('fields', $option)): ?>
                    <tr>
                        <td>
                            <label for="<?= $optionName ?>"<?= $hintTagAttr ?>>
                                <?= $option['title'] ?>
                            </label>:
                        </td>
                        
                        <?php $fields = $option['fields']; ?>
                        
                        <?php foreach ($fields as $field): ?>
                            <td class="adm-detail-content-cell-r">
                                <?php
                                    $value = COption::GetOptionString($mid, $field['name']);
                                    $params = [
                                        'type="' . $field['type'] . '"',
                                        'id="' . $field['name'] . '"',
                                        'name="' . $field['name'] . '"',
                                    ];
                                    
                                    foreach ($field['params'] as $paramName => $paramValue) {
                                        $params[] = is_int($paramName) ? $paramValue : "{$paramName}=\"{$paramValue}\"";
                                    }
                                    
                                    $hintTagAttr = null;
                                    if (isset($option['hint'])) {
                                        $hintTagAttr = 'title="' . $option['hint'] . '"';
                                        $params[] = $hintTagAttr;
                                        $hintTagAttr .= ' ' . $hintTagAttr;
                                    }
                                    
                                    if (in_array($field['type'], ['text', 'number', 'checkbox'])) {
                                        if ($field['type'] === 'checkbox' && $value) {
                                            $params[] = 'checked';
                                        }
                                        if (empty($value) && $field['type'] === 'checkbox') {
                                            $value = 1;
                                        }
                                        $params[] = 'value="' . $value . '"';
                                    }
                                ?>

                                <?php if (in_array($field['type'], ['text', 'number', 'checkbox'])): ?>
                                    <input <?= implode(' ', $params) ?>>
                                <?php endif; ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>

    <?php
        // Доступ к модулю
        $tabControl->Buttons();
    ?>

    <input type="submit"
           name="Update"<?= ($MOD_RIGHT < 'W') ? " disabled" : null ?>
           value="<?= GetMessage('MAIN_SAVE') ?>"
           class="adm-btn-save">
    <input type="reset"
           name="reset"
           value="<?= GetMessage('MAIN_RESET') ?>">
    <?php
        echo bitrix_sessid_post();
        $tabControl->End();
    ?>
</form>
