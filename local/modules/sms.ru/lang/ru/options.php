<?php
$MESS["SMSRU_OPTIONS_SETTING"] = 'Настройки';
$MESS["SMSRU_OPTIONS_TAB_TITLE"] = 'Настройки модуля sms.ru';
$MESS["SMSRU_OPTIONS_API_LABEL"] = 'api_id';
$MESS["SMSRU_OPTIONS_PHONE_LABEL"] = 'Телефон получателя';
$MESS["SMSRU_OPTIONS_TG_TOKEN_LABLE"] = 'Токен Telegram';
$MESS["SMSRU_OPTIONS_TG_CHAT_ID_LABEL"] = 'ID чата Telegram';
$MESS["SMSRU_OPTIONS_BALANCE"] = 'Баланс';
