<?php
namespace  SMSRU\Events;


use COption;

class Basic {
    const MID = 'sms.ru';
    public static function OnSaleOrderSaved(\Bitrix\Main\Event $event){
        $api_id = COption::GetOptionString(self::MID, 'api_id');

        if (!empty($api_id)) {

            /** @var \Bitrix\Sale\Order $order */
            $order = $event->getParameter("ENTITY");
            $isNew = $event->getParameter("IS_NEW");
            if ($isNew)
            {
                $id = $order->getId();
                foreach ($order->getPropertyCollection() as $prop) {
                    if ($prop->getField('CODE') == 'NAME') {
                       $name = $prop->getField('VALUE');
                    } else if ($prop->getField('CODE') == 'PHONE') {
                       $phone = $prop->getField('VALUE');
                    }
                }
                $basket = $order->getBasket();
                foreach ($basket as $basketItem) {
                    $productName = $basketItem->getField('NAME');
                    $productQuantity = $basketItem->getQuantity();
                }
                $price = $order->getPrice();
                //$delivery = \Bitrix\Sale\Delivery\Services\Manager::getById($order->getDeliveryIdList()[0]);
                $shipmentmentCollection = $order->getShipmentCollection();

                foreach ($shipmentmentCollection as $shipment)
                {
                    $delivery['NAME'] = $shipment->getField('DELIVERY_NAME');
                    $delivery['PRICE_DELIVERY'] = $shipment->getPrice();
                }
                if ($delivery['PRICE_DELIVERY'] == 0 && $delivery['NAME']== ''){
                    $delivery['NAME']  = 'Самовывоз';
                }
                $smsru = new \SMSRU($api_id);
                $data = new \stdClass();
                $data->to = COption::GetOptionString(self::MID, 'phone');
                $data->text = "Новый заказ № $id от $name $phone
"; // Текст сообщения
                $data->text.= "$productName - $productQuantity л, Сумма $price Руб,
доставка: ".$delivery['NAME'].": ".$delivery['PRICE_DELIVERY']." Руб";
                $sms = $smsru->send_one($data);
                if ($sms->status != "OK") { // Запрос выполнен успешно
                    AddMessage2Log("Код ошибки: $sms->status_code Текст ошибки: $sms->status_text", self::MID);
                }
                self::message_to_telegram($data->text);
            }
        }
    }
    protected static function message_to_telegram($text)
    {

        $TELEGRAM_TOKEN  = COption::GetOptionString(self::MID, 'tg_token');
        $TELEGRAM_CHAT_ID = COption::GetOptionString(self::MID, 'tg_chat');
        if (!empty($TELEGRAM_TOKEN) && !empty($TELEGRAM_CHAT_ID)){
            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL => 'https://api.telegram.org/bot' . $TELEGRAM_TOKEN . '/sendMessage',
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_POSTFIELDS => array(
                        'chat_id' => $TELEGRAM_CHAT_ID,
                        'text' => $text,
                    ),
                )
            );
            curl_exec($ch);
        }
    }
}