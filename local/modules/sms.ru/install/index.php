<?

use Bitrix\Main\Config\Option;
use Bitrix\Main\IO\FileNotFoundException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;

Loc::loadMessages(__FILE__);

class sms_ru extends \CModule {
    var $MODULE_ID = 'sms.ru';

    /** @var int */
    public $MODULE_SORT = -1; // that's means first point after main
    /** @var string */
    public $MODULE_VERSION;
    /** @var string */
    public $MODULE_VERSION_DATE;
    /** @var string */
    public $MODULE_NAME;
    /** @var string */
    public $MODULE_DESCRIPTION;
    /** @var string */
    public $MODULE_GROUP_RIGHTS;
    /** @var string */
    public $PARTNER_NAME;
    /** @var string */
    public $PARTNER_URI;


    /**
     * firstbit_reports constructor.
     */
    public function __construct() {
        $this
            ->setVersionInfo()
            ->setModuleInfo()
            ->setPartnerInfo();
    }

    private function setVersionInfo(): self {
        $version = @include(__DIR__ . "/version.php");

        $this->MODULE_VERSION      = $version['VERSION'] ?? Loc::getMessage('NOT_SPECIFIED');
        $this->MODULE_VERSION_DATE = $version['VERSION_DATE'] ?? Loc::getMessage('NOT_SPECIFIED');

        return $this;
    }

    private function setModuleInfo(): self {
        $this->MODULE_NAME         = Loc::getMessage('SMSRU_MODULE_NAME');
        $this->MODULE_DESCRIPTION  = Loc::getMessage('SMSRU_MODULE_DESC');
        $this->MODULE_GROUP_RIGHTS = 'N';

        return $this;
    }

    private function setPartnerInfo() {
        $this->PARTNER_NAME = Loc::getMessage('SMSRU_PARTNER_NAME');
        $this->PARTNER_URI  = Loc::getMessage('SMSRU_PARTNER_URI');

        return $this;
    }


    /**
     * Installation steps, and register it at system.
     * THIS METHOD NAME REQUIRED AS IS, DO NOT CHANGE IT.
     *
     * @return bool
     */
    public function DoInstall(): bool {


        ModuleManager::registerModule($this->MODULE_ID); // register module in system
        $this->registerEvents();
        Loader::includeModule($this->MODULE_ID);
        return true;
    }

    /**
     * Uninstall module step by step, and remove it from system.
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentNullException
     */
    public function DoUninstall() {
        ModuleManager::unRegisterModule($this->MODULE_ID);
        $this->unRegisterEvents();
    }

    /**
     * Install(copy) files(admin, images, components, wizards, etc..)
     *
     * @return self
     */
    public function InstallFiles(): self {
        return $this;
    }

    /**
     * Uninstall(copy) files(admin, images, components, wizards, etc..)
     *
     * @return bool
     */
    public function UnInstallFiles() {
        return true;
    }
    protected function registerEvents(){
        EventManager::getInstance()->registerEventHandler(
            "sale",
            "OnSaleOrderSaved",
            $this->MODULE_ID,
            \SMSRU\Events\Basic::class,
            'OnSaleOrderSaved'
        );
    }
    protected function unRegisterEvents(){
        EventManager::getInstance()->unRegisterEventHandler(
            "sale",
            "OnSaleOrderSaved",
            $this->MODULE_ID,
            \SMSRU\Events\Basic::class,
            'OnSaleOrderSaved'
        );
    }
}