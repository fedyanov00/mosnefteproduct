<?php

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

Class CDeliveryDistance {
    /**
     * Обьем от 100 до 30 000 литров. Доставка Мкад +3км (называется в пределах Москвы)
    Далее отсечка 20км до 100 км от МКАД. Итого отсечек по доставке: 3км от МКАД, 20, 40, 60, 80, 100. Свыше 100 обсуждается с менеджером.
    1) От 100 литров до 5000л
    Цена * количество литров + доставка (внутри МКАД 3000р, далее начиная с 3км +500р, каждые 20км от мкад)
    2) от 5001 литра до 15.000л
    Цена * количество литров + доставка (внутри МКАД 6000р, далее начиная с 3км +1000р, каждые 20км от мкад)
    3) от 15.001литра до 30.000л
    Цена * количество литров + доставка (внутри МКАД 10.000р, далее начиная с 3км +1.500р, каждые 20км от мкад)

     */
    function Init()
    {
        //настройки
        return array(
            "SID" => "Distance",  // Идентификатор службы доставки
            "NAME" => "Стоимость в зависимости от растояния",
            "DESCRIPTION" => "Рассчитывает стоимость доставки в зависимоти от работяния от МКАД и бъема топлива",
            "DESCRIPTION_INNER" => "Рассчитывает стоимость доставки в зависимоти от работяния от МКАД и бъема топлива",
            "BASE_CURRENCY" => "RUR",

            "HANDLER" => __FILE__,

            /* Определение методов */
            "DBGETSETTINGS" => array("CDeliveryDistance", "GetSettings"),
            "DBSETSETTINGS" => array("CDeliveryDistance", "SetSettings"),
            "GETCONFIG" => array("CDeliveryDistance", "GetConfig"),

            "COMPABILITY" => array("CDeliveryDistance", "Compability"),
            "CALCULATOR" => array("CDeliveryDistance", "Calculate"),

            /* Список профилей */
            "PROFILES" => array(
                "all" => array(
                    "TITLE" => "По расстоянию от МКАД",
                    "DESCRIPTION" => "Профиль доставки в зависимости от расстояния от МКАД",

                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
            )
        );
    }

    /* Установка параметров */
    function SetSettings($arSettings)
    {
        foreach ($arSettings as $key => $value) {
            if (strlen($value) > 0)
                $arSettings[$key] = doubleval($value);
            else
                unset($arSettings[$key]);
        }

        return serialize($arSettings);
    }

    /* Запрос параметров */
    function GetSettings($strSettings)
    {
        return unserialize($strSettings);
    }

    /* Запрос конфигурации службы доставки */
    function GetConfig()
    {
        $arConfig = array(
            "CONFIG_GROUPS" => array(
                "all" => "Параметры",
            ),
            "CONFIG" => array(
                "DELIVERY_PRICE_IN_MKAD" => array(
                    "TYPE" => "STRING",
                    "DEFAULT" => "3000",
                    "TITLE" => "Стоимость доставки внутри МКАД",
                    "GROUP" => "all"
                ),
                "MIN_FUEL" => array(
                    "TYPE" => "STRING",
                    "DEFAULT" => "100",
                    "TITLE" => "Минимальный объем",
                    "GROUP" => "all"
                ),
                "MAX_FUEL" => array(
                    "TYPE" => "STRING",
                    "DEFAULT" => "5000",
                    "TITLE" => "Максимальный объем",
                    "GROUP" => "all"
                ),
                "DELIVERY_PRICE_PER_MKAD" => array(
                    "TYPE" => "STRING",
                    "DEFAULT" => "500",
                    "TITLE" => "Стоимость, далее начиная с 3км за каждые 20км от мкад",
                    "GROUP" => "all"
                )
            ),
        );
        return $arConfig;
    }

    /* Проверка соответствия профиля доставки заказу */
    function Compability($arOrder, $arConfig)
    {
        return array("all");
    }

    /* Калькуляция стоимости доставки*/
    function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false)
    {
//        $deliveries = \Bitrix\Sale\Delivery\Services\Manager::getByParentId(13);
//        foreach ($deliveries as $delivery){
//            $deliveryParams[$delivery['ID']]  = unserialize(unserialize($delivery['CONFIG']['MAIN']['OLD_SETTINGS']));
//        }
//
        return array(
            "RESULT" => "OK",
            "VALUE" => $arConfig["DELIVERY_PRICE_IN_MKAD"]
        );
    }

    public static function getListDistance(): array
    {
        $result = [];
        CModule::IncludeModule('highloadblock');
        $hlblock = HL\HighloadBlockTable::getById(HL_DISTANCE_ID)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entityClass = $entity->getDataClass();

                $res = $entityClass::getList(array(
                    'select' => array('*'),
                    'order' => array('UF_DISTANCE' => 'ASC')
           ));

        while($row = $res->fetch()){
            $result[] = [
                'DISTANCE' => $row['UF_DISTANCE'],
                'NAME' => $row['UF_NAME']
            ];
        }

        return $result;
    }
}