<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Application;

$currentStatic = '91cdad7f248ee32f20f0';
Asset::getInstance()->addCss("/dist/main-$currentStatic.css");
Asset::getInstance()->addJs("/dist/main-$currentStatic.js");

?>
<!DOCTYPE html>
<html  lang="<?=Application::getInstance()->getContext()->getLanguage()?>">
    <head>
        <meta charset="UTF-8">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->ShowHead(); ?>
    </head>
    <body>
    <?$APPLICATION->ShowPanel()?>
    <?$APPLICATION->IncludeComponent("bitrix:menu","top-mobile",Array(
            "ROOT_MENU_TYPE" => "main",
            "MAX_LEVEL" => "1",
            "CHILD_MENU_TYPE" => "main",
            "USE_EXT" => "N",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "Y",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => ""
        )
    );?>

    <div class="content-wrapper">
        <!-- header -->
        <div class="page-header">
            <div class="page-header__search-icon"></div>
            <a class="logo page-header__logo" href="/"></a>
            <?$APPLICATION->IncludeComponent("bitrix:menu","top-main",Array(
                    "ROOT_MENU_TYPE" => "main",
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "main",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => ""
                )
            );?>
        </div>
        <!-- end header -->


