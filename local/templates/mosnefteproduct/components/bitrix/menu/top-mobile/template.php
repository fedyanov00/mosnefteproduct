<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)) : ?>
    <div data-mobile-btn class="mobile-menu__btn">
        <div class="mobile-menu__btn-bars">
            <div class="mobile-menu__btn-bar"></div>
            <div class="mobile-menu__btn-bar"></div>
            <div class="mobile-menu__btn-bar"></div>
        </div>
    </div>
    <div data-mobile-menu class="mobile-menu">
        <div class="mobile-menu__inner">
            <!-- mobile menu items -->
        <? foreach ($arResult as $keyItem => $arrItem) : ?>
            <a href="<?=$arrItem["LINK"]?>" class="mobile-menu__item" <?=(($arrItem['SELECTED']) ? 'data-is-selected' : '')?>><?=$arrItem["TEXT"]?></a>
        <?endforeach;?>
        </div>
    </div>
<?endif;?>
