<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)) : ?>
    <div class="page-header__desktop-menu">
        <div class="desktop-menu">
            <!-- desktop menu items -->
        <? foreach ($arResult as $keyItem => $arrItem) : ?>
            <a href="<?=$arrItem["LINK"]?>" class="desktop-menu__item" ><?=$arrItem["TEXT"]?></a>
        <?endforeach;?>
        </div>
    </div>
<?endif;?>
