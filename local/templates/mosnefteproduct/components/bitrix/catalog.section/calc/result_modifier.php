<?php

try {
    $arResult['DELIVERY'] = CDeliveryDistance::getListDistance();
    $deliveries = \Bitrix\Sale\Delivery\Services\Manager::getByParentId(DELIVERY_ID);
    foreach ($deliveries as $delivery){
        $arResult['DELIVERY_PARAMS'][$delivery['ID']]  = unserialize(unserialize($delivery['CONFIG']['MAIN']['OLD_SETTINGS']));
    }
} catch (\Bitrix\Main\ArgumentException $e) {
    global $APPLICATION;
    $APPLICATION->RestartBuffer();
    echo 'Не заданны службы доставки';
    exit();
}
