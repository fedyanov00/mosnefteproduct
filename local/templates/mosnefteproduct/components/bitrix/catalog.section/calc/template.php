<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 */
$this->setFrameMode(true);
?>

<div class="section">
    <div class="section__title">доставка топлива</div>
    <div class="section__content">
        <div class="cart">
            <div class="cart__content">
                <div class="calc" data-calc>
                    <form novalidate action="/create_order/" method="post">
                        <div class="form-group">
                        <?if (!empty($arResult['ITEMS'])):?>
                            <select data-fuel-type name="fuelType" class="form-select">
                                <option value="">Выберите тип топлива</option>
                                <?foreach ($arResult['ITEMS'] as $item):?>
                                    <option value="<?=$item['ID']?>" data-price="<?=$item['MIN_PRICE']['VALUE']?>"><?=$item['NAME']?></option>
                                <?endforeach;?>
                            </select>
                        <?endif;?>
                        </div>
                        <div class="form-group">
                            <input data-fuel-volume type="number" name="fuelVolume" placeholder="Объём топлива" min="100" data-max-volume-threshold="30000" value="100" required>
                        </div>
                        <div class="form-group">
                            <div class="calc__delivery-type">
                                <label class="radio-btn">
                                    <input data-delivery-type type="radio" name="delivery-type" value="delivery" checked>
                                    <span class="radio-btn__title">доставка</span>
                                </label>
                                <label class="radio-btn">
                                    <input data-delivery-type type="radio" name="delivery-type" value="pickup">
                                    <span class="radio-btn__title">самовывоз</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <select data-distance name="distance" class="form-select">
                            <?foreach ($arResult['DELIVERY'] as $delivery):?>
                                <option value="<?=$delivery['DISTANCE']?>" data-distance="<?=($delivery['DISTANCE'])?>" <?=($delivery['DISTANCE']==0) ? 'data-is-inside-mkad':''?> ><?=$delivery['NAME']?></option>
                            <?endforeach;?>
                                <option value="3" data-is-too-far-from-mkad>более 100 км от МКАД</option>
                            </select>
                            <input type="hidden" data-total-cost name="totalCost">

                            <?foreach ($arResult['DELIVERY_PARAMS'] as $PARAM): ?>
                                <input type="hidden"
                                       data-delivery-price-inside-mkad="<?=$PARAM['DELIVERY_PRICE_IN_MKAD']?>"
                                       data-delivery-price="<?=$PARAM['DELIVERY_PRICE_PER_MKAD']?>"
                                       data-min-fuel-volume="<?=$PARAM['MIN_FUEL']?>">
                            <?endforeach;?>
                        </div>
                        <div class="calc__results">
                            <div class="calc__result">
                                <div>Стоимость доставки</div>
                                <div class="calc__result-value">
                                    <span data-delivery-cost-output>1000</span><span class="calc__result-value-unit">&#8381;</span>
                                </div>
                            </div>
                            <div class="calc__result">
                                <div>Ваша цена с доставкой</div>
                                <div class="calc__result-value">
                                    <span data-price-with-delivery-output>36,78</span><span class="calc__result-value-unit">&#8381;/л</span>
                                </div>
                            </div>
                        </div>
                        <div class="calc__total">
                            <div class="product-total-cost">
                                <div class="product-total-cost__title">Итого</div>
                                <div class="product-total-cost__value">
                                    <span data-total-output>12000</span><span class="product-total-cost__unit">&#8381;</span>
                                </div>
                            </div>
                        </div>
                        <div class="calc__buttons">
                            <input data-hidden-input type="hidden" name="use-any-value-here">
                            <button style="display: none" type="button" class="btn btn--outlined btn--green" data-pay-online-btn data-hidden-input-value="1">Оплата онлайн</button>
                            <button type="button" class="btn btn--filled" data-send-request-btn data-hidden-input-value="2">Заявка на выбранный объём</button>
                        </div>
                        <input type="hidden" data-delivery-price value="100">
                        <input type="hidden" data-total-cost name="totalCost">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
