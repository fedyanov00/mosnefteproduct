<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 */
$this->setFrameMode(true);
?>
<div class="section">
    <div class="section__content">
        <div class="cart">
            <div class="cart__title">цены на нефтепродукты</div>
            <div class="cart__subtitle">на сегодня</div>
            <div class="cart__content" data-prices>

                <?if (!empty($arResult['ITEMS'])):?>
                    <select class="form-select" data-select>
                        <?foreach ($arResult['ITEMS'] as $item):?>
                            <option value="<?=$item['MIN_PRICE']['VALUE']?>"><?=$item['NAME']?></option>
                        <?endforeach;?>
                    </select>
                <?endif;?>
                <div class="price__view">
                    <div class="price__view-value">
                        <span data-value><?=$arResult['ITEMS'][0]['MIN_PRICE']['VALUE']?></span><span class="price__view-unit">&#8381;/л</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
