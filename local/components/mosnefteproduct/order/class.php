<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Catalog\Product\Basket;
use \Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Sale;
use \Bitrix\Main\SystemException;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\ORM\Fields\ExpressionField;

class AdditionalsCreate extends \CBitrixComponent
{

    protected $request;
    protected $basket;
    protected $order;

    public function __construct($component = null)
    {
        try {
            \Bitrix\Main\Loader::includeModule('sale');
            $this->request = Context::getCurrent()->getRequest();
            if (!$this->request->isPost()){
                LocalRedirect('/');
            }
        } catch (\Bitrix\Main\LoaderException $e) {
            ShowMessage(['Не подключен модуль sale']);
        }

        parent::__construct($component);
    }

    public function executeComponent()
    {
        if (!empty($this->request->getPost('use-any-value-here'))){
            if ($this->setBasket()){
                $this->createOrder();
                //$this->getPersons();
            }
            if (!$this->arResult['PERSONS']){
                $this->setPerson();
            }

            //Получение текущей карзины для вывода.
            $basket = $this->order->getBasket();
            $this->arResult['DELIVERY'] = \Bitrix\Sale\Delivery\Services\Manager::getById(13);

            //$this->arResult['BASKET'] = $basket->getBasketItems();
            foreach ($basket as $basketItem) {
                $this->arResult['BASKET']['PRODUCT_NAME'] = $basketItem->getField('NAME');
                $this->arResult['BASKET']['PRODUCT_QUANTITY'] = $basketItem->getQuantity();
            }
            $this->arResult['PRICE'] = $this->order->getPrice();
            $template = 'template';
        }elseif($this->request->getPost('user-agreements') == 'on'){
            $this->basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
            $this->createOrder();
            foreach ($this->order->getPropertyCollection() as $prop) {
                if ($prop->getField('CODE') == 'NAME') {
                    $value = $this->request->getPost('NAME');
                } else if ($prop->getField('CODE') == 'PHONE') {
                    $value = $this->request->getPost('PHONE');
                }
                else if ($prop->getField('CODE') == 'MAIL') {
                    $value = $this->request->getPost('MAIL');
                }
                if (!empty($value)) {
                    $prop->setValue($value);
                }
            }


            $this->order->save();
            $this->arResult['ORDER_ID'] = $this->order->getId();
            $template = 'success';
        }
        $this->includeComponentTemplate($template);
    }

    protected function setBasket(){
        $this->basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
        $this->basket->clearCollection();
        $values = $this->request->getPostList();
        if (!empty($values['fuelVolume'])){
            $productId =intval($values['fuelType']);
            $quantity = intval($values['fuelVolume']);
            if ($item =  $this->basket->getExistsItem('catalog', $productId)) {
                $item->setField('QUANTITY', $item->getQuantity() + $quantity);
            }
            else {
                $item = $this->basket->createItem('catalog', $productId);
                $item->setFields(array(
                    'QUANTITY' => $quantity,
                    'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                    'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
                    'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                ));
                /*
                Если потребуется добавить товар с произольной ценой:
                $item->setFields(array(
                    'QUANTITY' => $quantity,
                    'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                    'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
                    'PRICE' => $customPrice,
                    'CUSTOM_PRICE' => 'Y',
               ));
               */
            }
            $this->basket->save();
            return true;
        }
        return false;
    }
    protected function createOrder(){

//        $userId = $this->getUserId() ?: CSaleUser::GetAnonymousUserID();
//        $order = Sale\Order::create($this->getSiteId(), $userId);
//
//        $result = $order->appendBasket($basket);
//        if (!$result->isSuccess())
//        {
//            $this->errorCollection->add($result->getErrors());
//        }


        try {
            $this->order = \Bitrix\Sale\Order::create(Bitrix\Main\Context::getCurrent()->getSite(), Sale\Fuser::getId());
            $this->order->setBasket(
                $this->basket
            );
            $this->order->setPersonTypeId(Sale\Fuser::getId());

            if ($this->request->getPost('delivery-type')!='delivery'){
                $deliveryId = 0;
                $this->arResult['DELIVERY_TYPE'] = 'Самовывоз';
            }else{
                $distance = intval($this->request->getPost('distance'));
                $this->arResult['DISTANCE'] = $distance;
                $deliveryId = DELIVERY_ID;
            }
            $fuel = intval($this->request->getPost('fuelVolume'));
            // Добавляем отгрузку (службу доставки)
            /* @var $shipmentCollection \Bitrix\Sale\ShipmentCollection */
            $shipmentCollection = $this->order->getShipmentCollection();

            if ($distance != 0 && $deliveryId!=0) {

                $shipment = $shipmentCollection->createItem(
                    Bitrix\Sale\Delivery\Services\Manager::getObjectById(
                        DELIVERY_ID
                    )
                );
                $service = \Bitrix\Sale\Delivery\Services\Manager::getById(DELIVERY_ID);

                $deliveries = \Bitrix\Sale\Delivery\Services\Manager::getByParentId(DELIVERY_ID);
                foreach ($deliveries as $delivery){
                    $arDeliveries[$delivery['ID']]  = unserialize(unserialize($delivery['CONFIG']['MAIN']['OLD_SETTINGS']));
                }
                $deliveryPrice = 0;

                foreach ($arDeliveries as $arDelivery){
                    if ($fuel >= $arDelivery['MIN_FUEL'] && $fuel <= $arDelivery['MAX_FUEL']){
                        $deliveryPrice = $arDelivery['DELIVERY_PRICE_IN_MKAD'] + (round($distance/20) * $arDelivery['DELIVERY_PRICE_PER_MKAD']);
                        continue;
                    }
                }
                $this->arResult['deliveryPrice'] = $deliveryPrice;
                $deliveryData = [
                    'DELIVERY_ID' => $service['ID'],
                    'DELIVERY_NAME' => ($distance>0) ? $distance.' Км от Мкад':'В пределах МКАД',
                    'ALLOW_DELIVERY' => 'Y',
                    'PRICE_DELIVERY' => $deliveryPrice,
                    'CUSTOM_PRICE_DELIVERY' => 'Y'
                ];
                $shipment->setFields($deliveryData);
            } else {
                $shipment = $shipmentCollection->createItem();
            }


            //$shipment->setBasePriceDelivery($deliveryPrice, true);
            /** @var $shipmentItemCollection \Bitrix\Sale\ShipmentItemCollection */
            $shipmentItemCollection = $shipment->getShipmentItemCollection();
            $shipment->setField('CURRENCY', $this->order->getCurrency());

            foreach ($this->order->getBasket()->getOrderableItems() as $item) {
                /**
                 * @var $item \Bitrix\Sale\BasketItem
                 * @var $shipmentItem \Bitrix\Sale\ShipmentItem
                 * @var $item \Bitrix\Sale\BasketItem
                 */
                $shipmentItem = $shipmentItemCollection->createItem($item);
                $shipmentItem->setQuantity($item->getQuantity());
            }

        } catch (\Bitrix\Main\ArgumentOutOfRangeException $e) {
            echo $e->getMessage();
        } catch (\Bitrix\Main\NotImplementedException $e) {
            echo $e->getMessage();
        } catch (\Bitrix\Main\ObjectException $e) {
            echo $e->getMessage();
        } catch (\Bitrix\Main\ArgumentNullException $e) {
            echo $e->getMessage();
        }

        //$this->order->setPersonTypeId($personType);


    }

    protected function setPerson(){
        $personType = intval($this->request->getPost('personType'));
        if (!empty($pesonType)){
            $db_props = CSaleOrderProps::GetList(
                array("SORT" => "ASC"),
                array(
                    "PERSON_TYPE_ID" => $personType,
                ),
                false,
                false,
                array()
            );
            while ($props = $db_props->Fetch()){
//                global $APPLICATION;
//                $APPLICATION->RestartBuffer();
//                echo '<pre>';
//                var_dump($props);
//                echo '</pre>';
//                exit();
            }
        }

    }
    protected function getPersons(){
        $dbRes = \Bitrix\Sale\PersonType::getList();
        while ($apPerson = $dbRes->fetch()){
            $this->arResult['PERSONS'][$apPerson['ID']] = $apPerson;
        }
    }

}
