<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$APPLICATION->SetTitle("Заказ оформлен");
?>
<div class="section">
    <div class="section__title">Заказ оформлен</div>
    <div class="section__content">
        <h3>Спасибо! Ваш заказ № <?=$arResult['ORDER_ID']?> оформлен.</h3>
        <span>Ожидайте, в ближайшее время с вами свяжется наш оператор.</span><br>
        <a href="/">На главную</a>
    </div>
</div>
