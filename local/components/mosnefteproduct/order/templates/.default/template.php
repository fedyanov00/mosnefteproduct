<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

?>
<div class="section">
    <div class="section__title">оформление заявки</div>
    <div class="section__content">
        <div class="cart">
            <div class="cart__content">

                <form data-request-registration novalidate action="/create_order/" method="post">
                    <div class="inputs">
                        <div class="form-group">
                            <input type="text" name="NAME" placeholder="Ваше имя" required>
                            <div class="validation-errors">
                                <div class="validation-error" data-error="required">Обязательное поле</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="tel" name="PHONE" placeholder="Телефон" required>
                            <div class="validation-errors">
                                <div class="validation-error" data-error="required">Обязательное поле</div>
                                <div class="validation-error" data-error="phone">Некорретное значение номера телефона</div>
                            </div>
                        </div>
                    </div>

                    <div class="product-details">
                        <div class="product-details__title"><?=$arResult['BASKET']['PRODUCT_NAME']?></div>
                        <div class="product-details__details">
                            <div class="product-details__detail">
                                <div>Объем топлива :</div>
                                <div><?=$arResult['BASKET']['PRODUCT_QUANTITY']?> л</div>
                                <input type="hidden" name="fuelVolume" value="<?=$arResult['BASKET']['PRODUCT_QUANTITY']?>">
                            </div>
                            <div class="product-details__detail">
                                <div>Расстояние от МКАДа:</div>
                                <?if (!empty($arResult['DELIVERY_TYPE'])):?>
                                    <div><?=$arResult['DELIVERY_TYPE']?></div>
                                <?else:?>
                                    <?if (intval($arResult['DISTANCE']) > 0):?>
                                        <div><?=$arResult['DISTANCE']?></div>
                                        <input type="hidden" name="distance" value="<?=$arResult['DISTANCE']?>">
                                        <input type="hidden" name="delivery-type" value="delivery">
                                    <?else:?>
                                        <div>В пределах МКАД</div>
                                    <?endif;?>
                                <?endif;?>
                            </div>
                        </div>

                        <div class="product-details__total">
                            <div class="product-total-cost">
                                <div class="product-total-cost__title">Итого</div>
                                <div class="product-total-cost__value">
                                    <span data-total-output><?=$arResult['PRICE']?></span><span class="product-total-cost__unit">&#8381;</span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <label class="checkbox checkbox--with-top-margin">
                        <input type="checkbox" name="user-agreements" required>
                        <span class="checkbox__title">Согласен с обработкой персональных данных</span>
                    </label>


                    <div class="form-submit-btn">
                        <button type="submit" class="btn btn--filled">Отправить заявку</button>
                    </div>

                    <a href="/" class="back-btn back-btn--with-top-margin">Назад</a>

                </form>

            </div>
        </div>
    </div>
</div>