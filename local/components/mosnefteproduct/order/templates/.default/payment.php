<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

?>
<div class="section">
    <div class="section__title">Оплата online</div>
    <div class="section__content">
        <?if (!empty($arResult['PERSONS'])):?>
        <form data-online-payment action="/create_order/" method="post" class="online-payment">
            <input data-hidden-input type="hidden" name="personType">
            <?foreach ($arResult['PERSONS'] as $person):?>
                <button type="button" data-hidden-input-value="<?=$person['ID']?>" name="person_type" class="btn btn--filled"><?=$person['NAME']?></button>
            <?endforeach;?>
            <button type="button" data-hidden-input-value="3" class="btn btn--outlined">Отменить оплату</button>
        </form>
        <?endif;?>
    </div>
    <??>
    <div class="section">
        <div class="section__title">Оплата физлицо</div>
        <div class="section__content">
            <div class="cart">
                <div class="cart__content">

                    <form data-individual-payment novalidate action="/individual-payment" method="post">
                        <div class="inputs">
                            <div class="form-group">
                                <input type="text" name="name" placeholder="Ваше имя" pattern="^[а-яА-ЯёЁa-zA-Z\s-]+$" required>
                                <div class="validation-errors">
                                    <div class="validation-error" data-error="required">Обязательное поле</div>
                                    <div class="validation-error" data-error="pattern">Некорректное значение</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="tel" name="phone" placeholder="Телефон" required>
                                <div class="validation-errors">
                                    <div class="validation-error" data-error="required">Обязательное поле</div>
                                    <div class="validation-error" data-error="phone">Некорретное значение номера телефона</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" placeholder="E-mail" required>
                                <div class="validation-errors">
                                    <div class="validation-error" data-error="required">Обязательное поле</div>
                                    <div class="validation-error" data-error="email">Некорретное значение E-mail</div>
                                </div>
                            </div>
                        </div>

                        <div class="product-details">
                            <div class="product-details__title">ТАС (Калужский НПЗ) (-15С)</div>
                            <div class="product-details__details">
                                <div class="product-details__detail">
                                    <div>Объем топлива :</div>
                                    <div>100 л</div>
                                </div>
                                <div class="product-details__detail">
                                    <div>Расстояние от МКАДа:</div>
                                    <div>30 км</div>
                                </div>
                            </div>

                            <div class="product-details__total">
                                <div class="product-total-cost">
                                    <div class="product-total-cost__title">Итого</div>
                                    <div class="product-total-cost__value">
                                        <span data-total-output>12000</span><span class="product-total-cost__unit">&#8381;</span>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <label class="checkbox checkbox--with-top-margin">
                            <input type="checkbox" name="user-agreements" required>
                            <span class="checkbox__title">Согласен с обработкой персональных данных</span>
                        </label>


                        <div class="form-submit-btn">
                            <button type="submit" class="btn btn--outlined btn--green">Оплата картой банка онлайн</button>
                        </div>

                        <a href="#" class="back-btn back-btn--with-top-margin">Назад</a>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>