export const create = (formEl, cfg = {}) => {
  const {
    getInputValue = el => el.value
  } = cfg;

  let listeners = [];
  let isValid;

  const textInputsSelector = ['text', 'email', 'tel', 'number'].map(v => `input[type="${v}"][required]`).join(',');
  const inputs = [
    ...Array.from(formEl.querySelectorAll(textInputsSelector)).map((el) => {
      return wrapInput(el, { getInputValue });
    }),
    ...Array.from(formEl.querySelectorAll('input[type="checkbox"][required]')).map(wrapCheckbox)
  ];

  isValid = isFormValid();

  inputs.forEach((input) => {
    input.addListener(onInputChange);
  });

  return {
    isValid: () => isValid,
    addListener
  };

  function addListener(fn) {
    listeners.push(fn);

    return () => {
      listeners = listeners.filter(v => v !== fn);
    };
  }

  function notifyListeners() {
    listeners.forEach(fn => fn());
  }

  function isFormValid() {
    return inputs.reduce((acc, input) => (acc && input.isValid()), true);
  }

  function onInputChange() {
    const newValue = isFormValid();
    if (newValue === isValid) {
      return;
    }
    isValid = newValue;
    notifyListeners();
  }
};

function wrapCheckbox(el) {
  const validator = checkboxValidator(el);

  const listeners = [];
  let isPristine = true;
  let isValid = validator.validate();

  el.addEventListener('change', onChange, false);

  return {
    addListener,
    isValid: () => isValid
  };

  function addListener(fn) {
    listeners.push(fn);
  }

  function onChange() {
    isPristine = false;
    validate();
  }

  function validate() {
    isValid = validator.validate();
    notifyListeners();
  }

  function notifyListeners() {
    listeners.forEach(fn => fn());
  }
}

function wrapInput(el, { getInputValue }) {
  const parentNode = el.parentNode;

  const validator = getValidator(el);

  const listeners = [];
  let error = validator.validate(getInputValue);
  let isPristine = true;

  el.addEventListener('change', onChange, false);
  el.addEventListener('input', onInput, false);
  el.addEventListener('paste', onPaste, false);
  el.addEventListener('cut', onCut, false);

  return {
    addListener,
    isValid
  };

  function isValid() {
    return error === null;
  }

  function addListener(fn) {
    listeners.push(fn);
  }

  function notifyListeners() {
    listeners.forEach(fn => fn());
  }

  function onChange() {
    isPristine = false;
    validate();
  }

  function onPaste() {
    isPristine = false;
    validate();
  }

  function onCut() {
    isPristine = false;
    validate();
  }

  function onInput() {
    validate();
  }

  function validate() {
    setTimeout(updateUI, 0);

    const newError = validator.validate(getInputValue);
    if (newError === error) {
      return;
    }

    error = newError;
    notifyListeners();
  }

  function updateUI() {
    if (isPristine) {
      return;
    }

    const hideAllErrors = () => {
      parentNode.querySelectorAll('[data-error]').forEach((el) => {
        el.removeAttribute('data-is-active');
      });
    };

    if (isValid()) {
      el.removeAttribute('data-has-error');
      hideAllErrors();
      return;
    }

    el.setAttribute('data-has-error', '');
    hideAllErrors();

    const errorEl = parentNode.querySelector(`[data-error="${error}"]`);
    if (errorEl === null) {
      return;
    }

    errorEl.setAttribute('data-is-active', '');
  }
}

function getValidator(el) {
  switch (el.getAttribute('type')) {
    case 'text':
      return textFieldValidator(el);
    case 'email':
      return emailValidator(el);
    case 'tel':
      return phoneValidator(el);
  }
}

function textFieldValidator(el) {
  return {
    validate
  };

  function validate(getInputValue) {
    const value = getInputValue(el);

    if (value.length === 0) {
      return 'required';
    }

    const pattern = el.getAttribute('pattern');
    if (pattern === '' || pattern === null) {
      return null;
    }

    const regExp = new RegExp(pattern, 'gi');
    if (!regExp.test(value)) {
      return 'pattern';
    }

    return null;
  }
}

function emailValidator(el) {
  return {
    validate
  };

  function validate(getInputValue) {
    const value = getInputValue(el);

    if (value.length === 0) {
      return 'required';
    }

    if (!/^\w[\w\.\d]*@\w[\w\.\d]*$/gi.test(value)) {
      return 'email';
    }

    return null;
  }
}

function phoneValidator(el) {
  return {
    validate
  };

  function validate(getInputValue) {
    const value = getInputValue(el);

    // only +7XXXXXXXXXX

    if (value.length === 0 || value === '+7') {
      return 'required';
    }

    if (!/^\+7\d{10}$/gi.test(value)) {
      return 'phone';
    }

    return null;
  }
}

function checkboxValidator(el) {
  return {
    validate: () => el.checked
  };
}

