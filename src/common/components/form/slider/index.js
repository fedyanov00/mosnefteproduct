import 'range-slider/js/rSlider.min';
import 'range-slider/css/rSlider.min.css';

import * as noUiSlider from 'nouislider';
import 'nouislider/dist/nouislider.css';

import './styles.scss';

export const create = (el, options = {}) => {
  let isDestroyed = false;

  const { range, start, step = 1, onUpdate } = options;

  noUiSlider.create(el, {
    start: [start],
    connect: [true, false],
    range,
    tooltips: true,
    step
  });

  el.noUiSlider.on('update', (values, handle) => {
    const tooltip = el.querySelector('.noUi-tooltip');
    const v = Math.trunc(parseFloat(tooltip.innerText));
    tooltip.innerText = `${v}`;

    onUpdate(Math.trunc(parseFloat(values[handle])));
  });

  return {
    destroy
  };

  function destroy() {
    if (isDestroyed) {
      return;
    }
    destroy = true;
    el.noUiSlider.destroy();
  }


};

function fromHtmlToNode(html) {
  const div = document.createElement('div');
  div.innerHTML = html.trim();
  return div.firstChild;
}
