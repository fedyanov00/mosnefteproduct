import { onDomReady } from '../../utils';
import styles from './styles.scss';

const activeAttrName = 'data-active-mobile-menu';

onDomReady(() => {
  const html = document.querySelector('html');
  const body = document.querySelector('body');
  const btn = document.querySelector('[data-mobile-btn]');
  const menu = document.querySelector('[data-mobile-menu]');

  setTimeout(() => {
    html.setAttribute('data-mobile-menu-initialed', '');
    stateInactive();
  });

  function stateInactive() {
    const onBtnPressed = () => {
      btn.removeEventListener('click', onBtnPressed, false);
      stateActive();
    };
    btn.addEventListener('click', onBtnPressed, false);

    html.removeAttribute(activeAttrName);
    body.style.marginTop = '0';
  }

  function stateActive() {
    const onBtnPressed = () => {
      btn.removeEventListener('click', onBtnPressed, false);
      stateInactive();
    };
    btn.addEventListener('click', onBtnPressed, false);

    const mediaQueryList = window.matchMedia(`(min-width: ${styles.desktopLayoutMinWidth})`);
    const onMediaQueryChange = () => {
      mediaQueryList.removeEventListener('change', onMediaQueryChange, false);
      stateInactive();
    };
    mediaQueryList.addEventListener('change', onMediaQueryChange, false);

    html.setAttribute(activeAttrName, '');
    body.style.marginTop = `${menu.offsetHeight}px`;
  }
});
