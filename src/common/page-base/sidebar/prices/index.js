import '../../../components/cart';
import './styles.scss';
import { onDomReady } from '../../../utils';

onDomReady(() => {
  document.querySelectorAll('[data-prices-sidebar]').forEach(setup);
});

function setup(el) {
  const titleOutput = el.querySelector('[data-fuel-title-output]');

  const rubOutputs = el.querySelectorAll('[data-rubs]');
  const kopOutputs = el.querySelectorAll('[data-kops]');

  const btnsContainer = el.querySelector('[data-prices-btns]');
  const btns = btnsContainer.querySelectorAll(['input[type="radio"]']);
  const labels = [];
  btns.forEach((el, i) => {
    el.addEventListener('change', onChange);

    const id = `data-prices-btn-${i}`;
    el.setAttribute('id', id);

    const label = document.createElement('label');
    label.setAttribute('for', id);
    btnsContainer.appendChild(label);
    labels.push(label);
  });

  update(getSelectedBtn());

  function onChange() {
    update(getSelectedBtn());
  }

  function getSelectedBtn() {
    const count = btns.length;
    for (let i = 0; i < count; i++) {
      if (btns[i].checked) {
        return btns[i];
      }
    }
  }

  function update(selected) {
    const price = selected.getAttribute('value').replace(/,/g, '.');
    const title = selected.getAttribute('data-title');
    const id = selected.getAttribute('id');
    titleOutput.innerText = title;

    labels.forEach((label) => {
      if (label.getAttribute('for') === id) {
        label.setAttribute('data-selected', '');
      } else {
        label.removeAttribute('data-selected');
      }
    });

    const parts = price.split('.');
    const rubs = [...parts[0]];
    const kops = [...parts[1]];

      if (rubs.length === 1 || rubs[0] === '0') {
        rubOutputs[0].setAttribute('data-is-hidden', '');
        rubOutputs[1].innerText = rubs[0];
      } else {
        rubOutputs[0].removeAttribute('data-is-hidden');
        rubOutputs[0].innerText = rubs[0];
        rubOutputs[1].innerText = rubs[1];
      }

    kopOutputs[0].innerText = kops[0];
    kopOutputs[1].innerText = kops[1];
  }
}
