import '../../common/components/section';
import '../../common/components/cart';
import { onDomReady } from '../../common/utils';
import './styles.scss';

onDomReady(() => {
  document.querySelectorAll('[data-docs]').forEach(setup);
});

function setup(el) {
  el.querySelector('[data-btn]').addEventListener('click', () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }, false);
}
