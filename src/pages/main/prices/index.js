import '../../../common/components/section';
import '../../../common/components/cart';
import '../../../common/components/form';
import { onDomReady } from '../../../common/utils';
import './styles.scss';

onDomReady(() => {
  document.querySelectorAll('[data-prices]').forEach(setup);
});

function setup(el) {
  const select = el.querySelector('[data-select]');
  select.addEventListener('change', update, false);

  const valueContainer = el.querySelector('[data-value]');

  update();

  function update() {
    const option = select.querySelectorAll('option')[select.selectedIndex];
    valueContainer.innerText = option.value;
  }
}
