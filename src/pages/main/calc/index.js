import '../../../common/components/section';
import '../../../common/components/cart';
import '../../../common/components/form';
import '../../../common/components/buttons';
import '../../../common/components/product-total-cost';
import { onDomReady } from '../../../common/utils';
import { create as createSlider } from '../../../common/components/form/slider';
import './styles.scss';

const DELIVERY_BY_COMPANY = 'delivery';

onDomReady(() => {
  document.querySelectorAll('[data-calc]').forEach(setup);
});

function setup(el) {
  const form = el.querySelector('form');

  const hiddenInput = el.querySelector('[data-hidden-input]');

  const payOnlineBtn = form.querySelector('button[data-pay-online-btn]');
  const sendRequestBtn = form.querySelector('button[data-send-request-btn]');

  const fuelTypeSelector = el.querySelector('[data-fuel-type]');
  const fuelVolumeInput = el.querySelector('[data-fuel-volume]');
  const distanceSelector = el.querySelector('[data-distance]');
  const deliveryTypeInputs = el.querySelectorAll('[data-delivery-type]');

  const totalOutputs = el.querySelectorAll('[data-total-output]');
  const totalCostInput = el.querySelector('[data-total-cost]');
  const deliveryCostOutputs = el.querySelectorAll('[data-delivery-cost-output]');
  const priceWithDeliveryOutput = el.querySelector('[data-price-with-delivery-output]');
  const fuelVouleOutput = el.querySelector('[data-fuel-volume-output]');
  const partialPriceWithDeliveryOutput = el.querySelector('[data-partial-price-with-delivery-output]');

  payOnlineBtn.addEventListener('click', onSubmitBtnPressed, false);
  sendRequestBtn.addEventListener('click', onSubmitBtnPressed, false);

  fuelTypeSelector.addEventListener('change', updateUI, false);
  distanceSelector.addEventListener('change', updateUI, false);
  deliveryTypeInputs.forEach((input) => {
    input.addEventListener('change', updateUI, false);
  });
  fuelVolumeInput.addEventListener('input', updateUI, false);
  fuelVolumeInput.addEventListener('cut', updateUI, false);
  fuelVolumeInput.addEventListener('paste', updateUI, false);

  const fuelSliderEl = el.querySelector('[data-fuel-volume-slider]');
  let fuelVolumeSlider = null;

  const distanceOutput = el.querySelector('[data-distance-output]');
  const distanceSliderEl = el.querySelector('[data-distance-slider]');
  let distanceSlider = null;

  const deliveryTypeMobileGroup = el.querySelector('[data-delivery-type-mobile]');
  const deliveryTypeDesktopGroup = el.querySelector('[data-delivery-type-desktop]');

  let formData;// = getFormData();

  const createSliders = () => {
    formData = getFormData();

    let minValue;
    let maxValue;

    minValue = parseFloat(fuelSliderEl.getAttribute('data-min-value'));
    maxValue = parseFloat(fuelSliderEl.getAttribute('data-max-value'));

    fuelVolumeSlider = createSlider(fuelSliderEl, {
      start: formData.fuelVolume,
      range: {
        min: minValue,
        max: maxValue
      },
      format: {
        to: (v => Math.floor(v))
      },
      onUpdate: (fuelVolume) => {
        fuelVolumeInput.value = fuelVolume;
        updateUI();
      }
    });

    minValue = parseFloat(distanceSliderEl.getAttribute('data-min-value'));
    maxValue = parseFloat(distanceSliderEl.getAttribute('data-max-value'));

    distanceSlider = createSlider(distanceSliderEl, {
      start: formData.distanceData.distance,
      range: {
        min: minValue,
        max: maxValue
      },
      onUpdate: (v) => {
        Array.from(distanceSelector.querySelectorAll('option')).reverse().forEach((el) => {
          const minDistance = parseFloat(el.getAttribute('data-distance'));
          if (v >= minDistance) {
            el.setAttribute('selected', '');
          }
          else {
            el.removeAttribute('selected');
          }
          distanceSelector.setAttribute('data-selected-distance', v);
        });
        updateUI();
      }
    });
  };

  const destroySliders = () => {
    if (fuelVolumeSlider !== null) {
      fuelVolumeSlider.destroy();
      fuelVolumeSlider = null;
    }

    if (distanceSlider !== null) {
      distanceSlider.destroy();
      distanceSlider = null;
    }
  };

  const onMlqChanged = () => {
    if (mlq.matches) {
      createSliders();
    } else {
      destroySliders();
    }
    updateDeliveryTypeGroups();
  };

  const mlq = window.matchMedia('(min-width: 1220px)');
  mlq.addListener(onMlqChanged);
  if (mlq.matches) {
    createSliders();
  }

  const updateDeliveryTypeGroups = () => {
    const { activeGroup, inactiveGroup } = getDeliveryTypeGroups();

    const inactiveInputs = Array.from(inactiveGroup.querySelectorAll('input'));
    const activeInputs = Array.from(activeGroup.querySelectorAll('input'));

    inactiveInputs.forEach((inactiveInput) => {
      activeInputs
        .filter(activeInput => activeInput.value === inactiveInput.value)
        .forEach((activeInput) => {
          activeInput.checked = inactiveInput.checked;
        });
    });
  };

  updateDeliveryTypeGroups();

  setTimeout(updateUI);

  function updateUI() {
    const formData = getFormData();

    updateDeliveryCostUI(formData);
    updatePriceWithDeliveryUI(formData);
    updateTotalCostUI(formData);
    updatePayOnlineBtn(formData);
    updateSendRequestBtn(formData);
    updateNeedsConnectWithManager(formData);
    updateDataPartialPriceWithDeliveryOutput(formData);

    fuelVouleOutput.innerText = formData.fuelVolume;
    distanceOutput.innerText = formData.distanceData.selectedDistance;
  }

  function onSubmitBtnPressed(event) {
    const btn = event.currentTarget;
    hiddenInput.value = btn.getAttribute('data-hidden-input-value');
    form.submit();
  }

  function getFormData() {
    return {
      fuelPrice: getSelectedFuelPrice(),
      fuelVolume: getSelectedFuelVolume(),
      minAllowedFuelVolume: getMinAllowedFuelVolume(),
      fuelMaxVolumeThreshold: getFuelMaxVolumeThreshold(),
      isDeliveryByCompany: shouldDeliverByCompany(),
      deliveryPrice: getDeliveryPrice(),
      distanceData: getDistanceData()
    };
  }

  function getSelectedFuelPrice() {
    return {
      isSelected: true,
      price: parseFloat(fuelTypeSelector.options[fuelTypeSelector.selectedIndex].getAttribute('data-price')) || 0
    };
  }

  function getSelectedFuelVolume() {
    return parseFloat(fuelVolumeInput.value) || 0;
  }

  function getMinAllowedFuelVolume() {
    return parseFloat(fuelVolumeInput.getAttribute('min')) || 0;
  }

  function getFuelMaxVolumeThreshold() {
    const attrName = 'data-max-volume-threshold';
    const el = form.querySelector(`[${attrName}]`);
    if (el === null) {
      return 0;
    }
    return parseFloat(el.getAttribute(attrName)) || 0;
  }

  function shouldDeliverByCompany() {
    const { activeGroup } = getDeliveryTypeGroups();
    return Array.from(activeGroup.querySelectorAll('input'))
      .filter(el => el.value === DELIVERY_BY_COMPANY)
      .filter(el => el.checked).length > 0;
  }

  function getDeliveryPrice() {
    const arr = Array.from(form.querySelectorAll('[data-delivery-price]'))
      .map((el) => ({
        minFuelVolume: parseFloat(el.getAttribute('data-min-fuel-volume')) || 0,
        priceInsideMkad: parseFloat(el.getAttribute('data-delivery-price-inside-mkad')) || 0,
        value: parseFloat(el.getAttribute('data-delivery-price')) || 0
      }))
      .sort((a, b) => a.minFuelVolume > b.minFuelVolume);

    arr.unshift({
      ...arr[0],
      minFuelVolume: 0
    });
    return arr;
  }

  function getDistanceData() {
    const selectedOption = distanceSelector.options[distanceSelector.selectedIndex];

    return {
      isTooFarFromMkad: selectedOption.hasAttribute('data-is-too-far-from-mkad'),
      distance: parseFloat(selectedOption.getAttribute('data-distance')) || 0,
      selectedDistance: parseInt(distanceSelector.getAttribute('data-selected-distance')) || 0
    };
  }

  function updatePayOnlineBtn(formData) {
    if (!canPayOnline(formData)) {
      payOnlineBtn.setAttribute('disabled', '');
    } else {
      payOnlineBtn.removeAttribute('disabled');
    }
  }

  function updateSendRequestBtn(formData) {
    if (!canSendRequest(formData)) {
      sendRequestBtn.setAttribute('disabled', '');
    } else {
      sendRequestBtn.removeAttribute('disabled');
    }
  }

  function updateDeliveryCostUI(formData) {
    deliveryCostOutputs.forEach((el) => {
      el.innerText = `${calcDeliveryCost(formData)}`;
    });
  }

  function updatePriceWithDeliveryUI(formData) {
    const value = calcPriceWithDelivery(formData) || 0;
    priceWithDeliveryOutput.innerText = `${value.toFixed(2)}`;
  }

  function updateTotalCostUI(formData) {
    let value = calcTotalCost(formData);
    value = isNaN(value) ? 0 : value;

    totalOutputs.forEach((el => {
      el.innerText = `${value}`;
    }));
    totalCostInput.value = `${value}`;
  }

  function updateNeedsConnectWithManager(formData) {
    const {
      distanceData: {
        isTooFarFromMkad
      },
      fuelMaxVolumeThreshold,
      fuelVolume,
      isDeliveryByCompany
    } = formData;

    const attrName = 'data-needs-connect-with-manager';
    if ((isDeliveryByCompany && isTooFarFromMkad) || fuelVolume > fuelMaxVolumeThreshold) {
      el.setAttribute(attrName, '');
    } else {
      el.removeAttribute(attrName);
    }
  }

  function updateDataPartialPriceWithDeliveryOutput(formData) {
    const rubOutputs = partialPriceWithDeliveryOutput.querySelectorAll('[data-rubs]')
    const kopOutputs = partialPriceWithDeliveryOutput.querySelectorAll('[data-kops]')

    const cost = calcDeliveryCost(formData) || '0.00';

    const parts = `${cost}`.split('.');
    if(parts.length === 1) {
      parts.push('00')
    }

    const rubs = [...parts[0]];
    const kops = [...parts[1]];

    if (rubs.length === 1 || rubs[0] === '0') {
      rubOutputs[0].setAttribute('data-is-hidden', '');
      rubOutputs[1].innerText = rubs[0];
    } else {
      rubOutputs[0].removeAttribute('data-is-hidden');
      rubOutputs[0].innerText = rubs[0];
      rubOutputs[1].innerText = rubs[1];
    }

    kopOutputs[0].innerText = kops[0];
    kopOutputs[1].innerText = kops[1];
  }

  function getDeliveryTypeGroups() {
    let activeGroup;
    let inactiveGroup;
    if (mlq.matches) {
      activeGroup = deliveryTypeDesktopGroup;
      inactiveGroup = deliveryTypeMobileGroup;
    } else {
      activeGroup = deliveryTypeMobileGroup;
      inactiveGroup = deliveryTypeDesktopGroup;
    }

    return { activeGroup, inactiveGroup };
  }
}

function isFormValid(formData) {
  const {
    fuelPrice,
    fuelVolume,
    minAllowedFuelVolume,
    distanceData,
    isDeliveryByCompany
  } = formData;

  const result = fuelPrice.isSelected && fuelVolume >= minAllowedFuelVolume;

  if (!isDeliveryByCompany) {
    return result;
  }
  return result;
}

function canPayOnline(formData) {
  return isFormValid(formData)
    && !formData.distanceData.isTooFarFromMkad
    && formData.fuelVolume < formData.fuelMaxVolumeThreshold;
}

function canSendRequest(formData) {
  return isFormValid(formData);
}

function calcDeliveryCost(formData) {
  const {
    isDeliveryByCompany,
    deliveryPrice: deliveryPriceData,
    fuelVolume,
    distanceData: {
      distance
    }
  } = formData;

  if (!isDeliveryByCompany || (isNaN(fuelVolume) || fuelVolume === 0)) {
    return 0;
  }

  let deliveryPrice;
  deliveryPriceData.reverse();
  for (let i = 0; i < deliveryPriceData.length; i++) {
    deliveryPrice = deliveryPriceData[i];
    if (fuelVolume >= deliveryPrice.minFuelVolume) {
      break;
    }
  }
  deliveryPriceData.reverse();
  return deliveryPrice.priceInsideMkad + deliveryPrice.value * Math.ceil(distance / 20);
}

function calcTotalCost(formData) {
  const {
    fuelPrice,
    fuelVolume
  } = formData;

  if (!fuelPrice.isSelected || isNaN(fuelVolume) || fuelVolume === 0) {
    return 0;
  }

  return fuelPrice.price * fuelVolume + calcDeliveryCost(formData);
}

function calcPriceWithDelivery(formData) {
  const {
    fuelVolume,
    fuelPrice,
    isDeliveryByCompany
  } = formData;

  if (!isDeliveryByCompany && fuelPrice.isSelected) {
    return fuelPrice.price;
  }

  if (isDeliveryByCompany && (!fuelPrice.isSelected || isNaN(fuelVolume) || fuelVolume === 0)) {
    return 0;
  }

  return Math.floor((calcTotalCost(formData) / fuelVolume * 100)) / 100;
}
