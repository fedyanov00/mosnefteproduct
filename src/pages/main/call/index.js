import IMask from 'imask';
import '../../../common/components/section';
import '../../../common/components/cart';
import '../../../common/components/form';
import { onDomReady } from '../../../common/utils';
import { create as createForm } from '../../../common/components/form';

import './styles.scss';

onDomReady(() => {
  document.querySelectorAll('[data-request-call-section]').forEach((el) => {
    const btn = el.querySelector('[data-btn]');
    const formTpl = document.querySelector('[data-form]');

    btn.addEventListener('click', onBtnPressed, false);

    function onBtnPressed(event) {
      event.preventDefault();
      showPopup();
    }

    function showPopup() {
      const hasActivePopupAttrName = 'data-is-active-popup';

      const body = document.querySelector('body');
      body.setAttribute(hasActivePopupAttrName, '');

      const div = document.createElement('div');

      div.innerHTML = formTpl.outerHTML.trim();
      const formNode = div.firstChild;
      body.appendChild(formNode);

      div.innerHTML = '<div class="call__popup-bg"></div>';
      const bg = div.firstChild;
      body.appendChild(bg);

      const cancelBtn = formNode.querySelector('[data-cancel-btn]');
      const submitBtn = formNode.querySelector('[data-submit-btn]');

      bg.addEventListener('click', onBgPressed, false);
      cancelBtn.addEventListener('click', onCancelBtnPressed, false);
      submitBtn.addEventListener('click', onSubmitBtnPressed, false);

      const phoneEl = formNode.querySelector('input[type="tel"]');
      const maskOptions = {
        mask: '{+7}(000)000-00-00',
        lazy: false
      };
      const mask = IMask(phoneEl, maskOptions);

      const form = createForm(formNode, {
        getInputValue: (el) => {
          if (el !== phoneEl) {
            return el.value;
          }
          return mask.unmaskedValue;
        }
      });
      const unsubscribeFromForm = form.addListener(updateUI);

      updateUI();

      function updateUI() {
        if (form.isValid()) {
          submitBtn.removeAttribute('disabled');
        } else {
          submitBtn.setAttribute('disabled', '');
        }
      }

      function closePopup() {
        unsubscribeFromForm();
        bg.removeEventListener('click', onBgPressed, false);
        cancelBtn.removeEventListener('click', onCancelBtnPressed, false);
        submitBtn.removeEventListener('click', onSubmitBtnPressed, false);

        body.removeChild(formNode);
        body.removeChild(bg);
        body.removeAttribute(hasActivePopupAttrName);
      }

      function onBgPressed() {
        closePopup();
      }

      function onCancelBtnPressed() {
        closePopup();
      }

      function onSubmitBtnPressed() {
        formNode.submit();
      }
    }
  });
});
