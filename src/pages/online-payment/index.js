import { onDomReady } from '../../common/utils';
import '../../common/components/buttons';
import './styles.scss';

onDomReady(() => {
  document.querySelectorAll('[data-online-payment]').forEach(setup);
});

function setup(formEl) {
  const hiddenInput = formEl.querySelector('[data-hidden-input]');

  formEl.querySelectorAll('button').forEach((btn) => {
    btn.addEventListener('click', onBtnPressed, false);
  });

  function onBtnPressed(event) {
    const btn = event.currentTarget;
    hiddenInput.value = btn.getAttribute('data-hidden-input-value');
    formEl.submit();
  }
}
