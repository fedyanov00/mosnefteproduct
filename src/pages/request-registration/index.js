import IMask from 'imask';
import '../../common/components/section';
import '../../common/components/cart';
import '../../common/components/buttons';
import '../../common/components/product-details';
import '../../common/components/back-btn';
import '../../common/components/user-agreements-checkbox';
import { onDomReady } from '../../common/utils';
import { create as createForm } from '../../common/components/form';
import './styles.scss';

onDomReady(() => {
  document.querySelectorAll('[data-request-registration]').forEach(setup);
});

function setup(formEl) {
  const submitBtn = formEl.querySelector('button[type="submit"]');

  const phoneEl = formEl.querySelector('input[type="tel"]');
  const maskOptions = {
    mask: '{+7}(000)000-00-00',
    lazy: false
  };
  const mask = IMask(phoneEl, maskOptions);

  const form = createForm(formEl, {
    getInputValue: (el) => {
      if (el !== phoneEl) {
        return el.value;
      }
      return mask.unmaskedValue;
    }
  });
  form.addListener(updateUI);

  updateUI();

  function updateUI() {
    if (form.isValid()) {
      submitBtn.removeAttribute('disabled');
    } else {
      submitBtn.setAttribute('disabled', '');
    }
  }
}
