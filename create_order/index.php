<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Создание заказа - МосНефтеПродукт");
$APPLICATION->IncludeComponent(
    "mosnefteproduct:order",
    "",
    Array(
        "SEF_MODE" => "N",
        "IBLOCK_TYPE_ID" => "catalog",
        "ACTION_VARIABLE" => "action",
        "CACHE_TIME" => 1*24*60*60,
        "BASKET_PAGE_TEMPLATE" => "/personal/basket.php",
    )
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");