require('dotenv').config();

const path = require('path');
const os = require('os');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlBeautifyPlugin = require('@nurminen/html-beautify-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

const host = os.platform() === 'linux' ? '0.0.0.0' : '127.0.0.1';
const port = 3000;

const projectDir = __dirname;
const srcDir = path.resolve(projectDir, 'src');
const outDir = path.resolve(projectDir, 'dist');

let publicPath = '/';
const altPublicPath = process.env.PUBLIC_PATH;
if (isProduction && typeof altPublicPath === 'string' && altPublicPath.length > 0) {
  publicPath = altPublicPath;
}

const commonPlugins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV)
    }
  }),
  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: path.resolve(srcDir, 'page-list.html'),
    chunks: [],
    minify: false
  }),
  new HtmlWebpackPlugin({
    filename: 'main.html',
    template: path.resolve(srcDir, 'pages/main/index.twig'),
    minify: false,
    publicPath: '/'
  }),
  new HtmlWebpackPlugin({
    filename: 'request-registration.html',
    template: path.resolve(srcDir, 'pages/request-registration/index.twig'),
    minify: false,
    publicPath: '/'
  }),
  new HtmlWebpackPlugin({
    filename: 'online-payment.html',
    template: path.resolve(srcDir, 'pages/online-payment/index.twig'),
    minify: false,
    publicPath: '/'
  }),
  new HtmlWebpackPlugin({
    filename: 'individual-payment.html',
    template: path.resolve(srcDir, 'pages/individual-payment/index.twig'),
    minify: false,
    publicPath: '/'
  }),
  new HtmlWebpackPlugin({
    filename: 'legal-entity-payment.html',
    template: path.resolve(srcDir, 'pages/legal-entity-payment/index.twig'),
    minify: false,
    publicPath: '/'
  }),
  new HtmlWebpackPlugin({
    filename: 'contacts.html',
    template: path.resolve(srcDir, 'pages/contacts/index.twig'),
    minify: false,
    publicPath: '/'
  }),
  new HtmlWebpackPlugin({
    filename: 'docs.html',
    template: path.resolve(srcDir, 'pages/docs/index.twig'),
    minify: false,
    publicPath: '/'
  }),
  new HtmlWebpackPlugin({
    filename: 'staff.html',
    template: path.resolve(srcDir, 'pages/staff/index.twig'),
    minify: false,
    publicPath: '/'
  }),
  new HtmlWebpackPlugin({
    filename: 'person.html',
    template: path.resolve(srcDir, 'pages/person/index.twig'),
    minify: false,
    publicPath: '/'
  })
];

const productionPlugins = [
  new CleanWebpackPlugin(),
  new MiniCssExtractPlugin({
    filename: '[name]-[chunkhash].css'
  }),
  new HtmlBeautifyPlugin({
    config: {
      end_with_newline: true,
      indent_size: 2,
      preserve_newlines: false
    }
  })
];

const miniCssExtractPluginLoaderCfg = {
  loader: MiniCssExtractPlugin.loader
};

const styleLoaderCfg = {
  loader: 'style-loader'
};

const cssLoaderCfg = {
  loader: 'css-loader',
  options: {
    sourceMap: !isProduction,
    importLoaders: 3,
    modules: {
      mode: 'global',
      localIdentName: isProduction ? '[hash:base64]' : '[path][name]__[local]',
      localIdentContext: srcDir
    }
  }
};

const resolveUrlLoaderCfg = {
  loader: 'resolve-url-loader',
  options: {
    sourceMap: !isProduction
  }
};

const postCssLoaderCfg = {
  loader: 'postcss-loader'
};

const sassLoaderCfg = {
  loader: 'sass-loader',
  options: {
    sourceMap: true // it's required for resolve-url-loader
  }
};

const assetNamePattern = 'assets/[name]_[hash].[ext]';

const imageFilesLoaders = [
  {
    loader: 'url-loader',
    options: {
      limit: 50000,
      name: assetNamePattern
    }
  }
];

if (isProduction) {
  imageFilesLoaders.push({
    loader: 'image-webpack-loader'
  });
}

module.exports = {
  context: projectDir,
  target: 'web',
  mode: isProduction ? 'production' : 'development',
  entry: {
    main: './src/index.js'
  },
  output: {
    path: outDir,
    publicPath,
    filename: isProduction ? '[name]-[chunkhash].js' : '[name].js',
    chunkFilename: isProduction ? '[name]-[chunkhash].chunk.js' : '[name].chunk.js'
  },
  devtool: isProduction ? false : 'source-map',
  module: {
    noParse: [
      /\.min\.js/
    ],
    rules: [
      {
        test: /\.js?$/,
        exclude: [
          /node_modules/
        ],
        use: [
          'babel-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          isProduction ? miniCssExtractPluginLoaderCfg : styleLoaderCfg,
          cssLoaderCfg,
          postCssLoaderCfg,
          resolveUrlLoaderCfg
        ]
      },
      {
        test: /\.(scss|sass)$/,
        use: [
          isProduction ? miniCssExtractPluginLoaderCfg : styleLoaderCfg,
          cssLoaderCfg,
          postCssLoaderCfg,
          resolveUrlLoaderCfg,
          sassLoaderCfg
        ]
      },
      {
        test: /\.(gif|png|jpe?g|ico|svg)$/,
        use: imageFilesLoaders
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: assetNamePattern
            }
          }
        ]
      },
      {
        test: /\.twig$/,
        use: [
          'raw-loader',
          'twig-html-loader'
        ]
      }
    ]
  },
  plugins: [
    ...commonPlugins,
    ...(isProduction ? productionPlugins : [])
  ],
  performance: {
    hints: false
  },
  devServer: {
    host,
    port,
    historyApiFallback: true,
    stats: 'minimal',
    contentBase: srcDir,
    disableHostCheck: true
  }
};
